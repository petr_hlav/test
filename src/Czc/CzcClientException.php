<?php

namespace HPTronic\Project\Czc;

use HPTronic\Project\GrabberException;

class CzcClientException extends GrabberException
{
}
