<?php

namespace HPTronic\Project\Czc;

use HPTronic\Project\IGrabber;
use HPTronic\Project\Metadata;

class CzcGrabber implements IGrabber
{

    /**
     * @var CzcProductFinder
     */
    private $productFinder;

    /**
     * @var CzcMetaFinder
     */
    private $priceFinder;

    /**
     * @param CzcProductFinder $productFinder
     * @param CzcMetaFinder $priceFinder
     */
    public function __construct(CzcProductFinder $productFinder, CzcMetaFinder $priceFinder)
    {
        $this->productFinder = $productFinder;
        $this->priceFinder = $priceFinder;
    }

    /**
     * @inheritDoc
     */
    public function getMetadata(string $productId): Metadata
    {
        $url = $this->productFinder->findProductUrlByCode($productId);

        return $this->priceFinder->findMetadataFromProductUrl($url);
    }
}
