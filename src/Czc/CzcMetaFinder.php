<?php

namespace HPTronic\Project\Czc;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use HPTronic\Project\Metadata;

class CzcMetaFinder
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var CzcProductParser
     */
    private $parser;

    /**
     * @param Client $client
     * @param CzcProductParser $parser
     */
    public function __construct(Client $client, CzcProductParser $parser)
    {
        $this->client = $client;
        $this->parser = $parser;
    }

    /**
     * @param string $url
     * @return Metadata
     * @throws CzcClientException
     * @throws ParserException
     */
    public function findMetadataFromProductUrl(string $url): Metadata
    {
        $request = new Request('GET', $url);

        try {
            $response = $this->client->send($request);
        } catch (GuzzleException $e) {
            throw new CzcClientException($e->getMessage());
        }

        $content = $response->getBody()->getContents();

        $price = $this->parser->priceFromHtml($content);
        $name = $this->parser->nameFromHtml($content);
        $rating = $this->parser->ratingFromHtml($content);

        return new Metadata($price, $name, $rating);
    }
}
