<?php

namespace HPTronic\Project\Czc;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class CzcProductFinder
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var CzcSearchParser
     */
    private $parser;

    /**
     * @param Client $client
     * @param CzcSearchParser $parser
     */
    public function __construct(Client $client, CzcSearchParser $parser)
    {
        $this->client = $client;
        $this->parser = $parser;
    }

    /**
     * @param string $code
     * @return string
     * @throws CzcClientException
     * @throws ParserException
     */
    public function findProductUrlByCode(string $code): string
    {
        $url = sprintf('https://www.czc.cz/%s/hledat', $code);

        $request = new Request('GET', $url);

        try {
            $response = $this->client->send($request);
        } catch (GuzzleException $e) {
            throw new CzcClientException($e->getMessage());
        }

        return $this->parser->urlFromHtml($response->getBody()->getContents());
    }
}
