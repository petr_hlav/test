<?php

namespace HPTronic\Project\Czc;

use DOMDocument;
use DOMXPath;

class CzcProductParser
{

    public function priceFromHtml(string $html): float
    {
        $document = new DOMDocument();
        @$document->loadHTML($html);

        $xpath = new DOMXPath($document);

        $query = $xpath->query(
            '//div[@id="product-price-and-delivery-section"]//span[@class="price action"]/span[@class="price-vatin"]'
        );

        if (!$query->count()) {
            throw new ParserException('Price not found');
        }

        $stringPrice = $query->item(0)->nodeValue;
        $stringPrice = preg_replace('/\D/', '', $stringPrice);

        return floatval($stringPrice);
    }

    public function nameFromHtml(string $html): string
    {
        $document = new DOMDocument();
        @$document->loadHTML($html);

        $xpath = new DOMXPath($document);

        $query = $xpath->query(
            '//div[@id="product-detail"]//h1'
        );

        if (!$query->count()) {
            throw new ParserException('Name not found');
        }

        return $query->item(0)->getAttribute('aria-label');
    }

    public function ratingFromHtml(string $html): ?int
    {
        $document = new DOMDocument();
        @$document->loadHTML($html);

        $xpath = new DOMXPath($document);

        $query = $xpath->query(
            '//div[@id="product-detail"]//span[@class="rating__label"]'
        );

        if (!$query->count()) {
            return null;
        }

        $stringPrice = $query->item(0)->nodeValue;
        $stringPrice = preg_replace('/\D/', '', $stringPrice);

        return intval($stringPrice);
    }
}
