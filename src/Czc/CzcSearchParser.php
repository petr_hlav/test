<?php

namespace HPTronic\Project\Czc;

use DOMDocument;
use DOMXPath;

class CzcSearchParser
{

    public function urlFromHtml(string $html): string
    {
        $document = new DOMDocument();
        @$document->loadHTML($html);

        $xpath = new DOMXPath($document);

        $query = $xpath->query('//div[@id="tiles"]//a[@class="tile-link"]');

        if (!$query->count()) {
            throw new ParserException('Url not found');
        }

        return 'http://www.czc.cz' . $query->item(0)->getAttribute('href');
    }
}
