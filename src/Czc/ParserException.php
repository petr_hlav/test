<?php

namespace HPTronic\Project\Czc;

use HPTronic\Project\GrabberException;

class ParserException extends GrabberException
{
}
