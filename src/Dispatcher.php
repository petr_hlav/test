<?php

namespace HPTronic\Project;

class Dispatcher
{

    /**
     * @var IGrabber
     */
    private $grabber;

    /**
     * @var IOutput
     */
    private $output;

    /**
     * @param IGrabber $grabber
     * @param IOutput $output
     */
    public function __construct(IGrabber $grabber, IOutput $output)
    {
        $this->grabber = $grabber;
        $this->output = $output;
    }

    /**
     * @return string JSON
     */
    public function run()
    {
        $filename = __DIR__ . '/vstup.txt';
        $input = new Input();

        foreach ($input->get($filename) as $productId) {
            try {
                $metadata = $this->grabber->getMetadata($productId);
                $this->output->add($productId, $metadata);
            } catch (GrabberException $e) {
                fwrite(STDERR, "Failed to process $productId: " . $e->getMessage() . PHP_EOL);
            }
        }

        return $this->output->getJson();
    }
}
