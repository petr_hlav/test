<?php

namespace HPTronic\Project;

interface IGrabber
{

    /**
     * @param string $productId
     * @return Metadata
     * @throws GrabberException
     */
    public function getMetadata(string $productId): Metadata;
}
