<?php

namespace HPTronic\Project;

interface IOutput
{

    public function add(string $productId, Metadata $metadata): void;

    /**
     * @return string
     */
    public function getJson();
}
