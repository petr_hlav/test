<?php

namespace HPTronic\Project;

class Input
{

    public function get(string $filename): array
    {
        $input = [];
        $fileContent = file_get_contents($filename);
        $explodedContent = explode("\n", $fileContent);

        foreach ($explodedContent as $productId) {
            $productId = trim($productId);

            if ($productId !== '') {
                $input[] = $productId;
            }
        }

        return $input;
    }
}
