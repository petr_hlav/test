<?php

namespace HPTronic\Project;

class JsonOutput implements IOutput
{

    /**
     * @var array
     */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    public function add(string $productId, Metadata $metadata): void
    {
        $this->items[$productId] = [
            'price' => $metadata->price(),
            'name' => $metadata->name(),
            'rating' => $metadata->rating(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getJson()
    {
        return json_encode($this->items, JSON_PRETTY_PRINT);
    }
}
