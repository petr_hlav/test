<?php

namespace HPTronic\Project;

class Metadata
{

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int|null
     */
    private $rating;

    /**
     * @param float $price
     * @param string $name
     * @param int|null $rating
     */
    public function __construct(float $price, string $name, ?int $rating)
    {
        $this->price = $price;
        $this->name = $name;
        $this->rating = $rating;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function rating(): ?int
    {
        return $this->rating;
    }
}
