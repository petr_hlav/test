<?php

use GuzzleHttp\Client;
use HPTronic\Project\Dispatcher;
use HPTronic\Project\Czc;
use HPTronic\Project\JsonOutput;

require __DIR__ . '/../vendor/autoload.php';

$httpClient = new Client();
$searchParser = new Czc\CzcSearchParser();
$productParser = new Czc\CzcProductParser();
$productFinder = new Czc\CzcProductFinder($httpClient, $searchParser);
$priceFinder = new Czc\CzcMetaFinder($httpClient, $productParser);
$grabber = new Czc\CzcGrabber($productFinder, $priceFinder);
$output = new JsonOutput();

$dispatcher = new Dispatcher($grabber, $output);
$response = $dispatcher->run();

echo $response;
