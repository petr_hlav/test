<?php

namespace HPTronic\Project\Test\Czc;

use HPTronic\Project\Czc\CzcGrabber;
use HPTronic\Project\Czc\CzcMetaFinder;
use HPTronic\Project\Czc\CzcProductFinder;
use HPTronic\Project\Metadata;
use PHPUnit\Framework\TestCase;

class CzcGrabberTest extends TestCase
{

    public function testGrab()
    {
        $code = '13455';
        $url = 'url';
        $metadata = new Metadata(100.6, 'Product', 56);

        $productFinder = $this->createMock(CzcProductFinder::class);
        $productFinder->expects($this->once())->method('findProductUrlByCode')->with($code)->willReturn($url);

        $priceFinder = $this->createMock(CzcMetaFinder::class);
        $priceFinder->expects($this->once())->method('findMetadataFromProductUrl')->with($url)->willReturn($metadata);

        $grabber = new CzcGrabber($productFinder, $priceFinder);

        $this->assertEquals($metadata, $grabber->getMetadata($code));
    }
}
