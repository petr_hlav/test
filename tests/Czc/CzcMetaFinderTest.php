<?php

namespace HPTronic\Project\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use HPTronic\Project\Czc\CzcMetaFinder;
use HPTronic\Project\Czc\CzcProductParser;
use HPTronic\Project\Metadata;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class CzcMetaFinderTest extends TestCase
{

    public function testFindUrl()
    {
        $code = '444123';
        $html = 'htmlCode';
        $price = 300.0;
        $name = 'Product name';
        $rating = 56;
        $metadata = new Metadata($price, $name, $rating);
        $expectedRequest = new Request('GET', $price);
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())->method('getContents')->willReturn($html);
        $clientResponse = $this->createMock(ResponseInterface::class);
        $clientResponse->expects($this->once())->method('getBody')->willReturn($stream);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())->method('send')->with($expectedRequest)->willReturn($clientResponse);

        $parser = $this->createMock(CzcProductParser::class);
        $parser->expects($this->once())->method('priceFromHtml')->with($html)->willReturn($price);
        $parser->expects($this->once())->method('nameFromHtml')->with($html)->willReturn($name);
        $parser->expects($this->once())->method('ratingFromHtml')->with($html)->willReturn($rating);

        $finder = new CzcMetaFinder($client, $parser);

        $this->assertEquals($metadata, $finder->findMetadataFromProductUrl($price));
    }
}
