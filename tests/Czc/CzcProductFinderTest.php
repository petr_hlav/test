<?php

namespace HPTronic\Project\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use HPTronic\Project\Czc\CzcProductFinder;
use HPTronic\Project\Czc\CzcSearchParser;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class CzcProductFinderTest extends TestCase
{

    public function testFindUrl()
    {
        $code = '444123';
        $html = 'htmlCode';
        $url = 'expectedUrl';
        $expectedRequest = new Request('GET', "https://www.czc.cz/$code/hledat");
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())->method('getContents')->willReturn($html);
        $clientResponse = $this->createMock(ResponseInterface::class);
        $clientResponse->expects($this->once())->method('getBody')->willReturn($stream);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())->method('send')->with($expectedRequest)->willReturn($clientResponse);

        $parser = $this->createMock(CzcSearchParser::class);
        $parser->expects($this->once())->method('urlFromHtml')->with($html)->willReturn($url);

        $finder = new CzcProductFinder($client, $parser);

        $this->assertEquals($url, $finder->findProductUrlByCode($code));
    }
}
