<?php

namespace HPTronic\Project\Test;

use HPTronic\Project\Czc\CzcProductParser;
use PHPUnit\Framework\TestCase;

class CzcCzProductParserTest extends TestCase
{

    public function testParsePrice()
    {
        $html = file_get_contents(__DIR__ . '/product.html');

        $parser = new CzcProductParser();
        $price = $parser->priceFromHtml($html);

        $this->assertEquals(7252.0, $price);
    }

    public function testParseName()
    {
        $html = file_get_contents(__DIR__ . '/product.html');

        $parser = new CzcProductParser();
        $price = $parser->nameFromHtml($html);

        $this->assertEquals('Intel Xeon E3-1230 v5', $price);
    }

    public function testParseRating()
    {
        $html = file_get_contents(__DIR__ . '/product.html');

        $parser = new CzcProductParser();
        $price = $parser->ratingFromHtml($html);

        $this->assertEquals(95, $price);
    }
}
