<?php

namespace HPTronic\Project\Test;

use HPTronic\Project\Czc\CzcSearchParser;
use PHPUnit\Framework\TestCase;

class CzcCzSearchParserTest extends TestCase
{

    public function testParseUrl()
    {
        $html = file_get_contents(__DIR__ . '/search.html');

        $parser = new CzcSearchParser();
        $url = $parser->urlFromHtml($html);

        $this->assertEquals('http://www.czc.cz/intel-xeon-e3-1230-v5/184529/produkt', $url);
    }
}
