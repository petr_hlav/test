<?php

namespace HPTronic\Project\Test;

use HPTronic\Project\Input;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{

    public function testReadInput()
    {
        $input = new Input();
        $filename = __DIR__ . '/input.txt';
        $expectedInput = [
            'BX80662E31230V5',
            'FD-CA-NODE-804-BL-W',
            'AA-46-BFGCD12443',
            '220-G2-0550-Y2',
            'NH-U9S',
        ];

        $this->assertEquals($expectedInput, $input->get($filename));
    }
}
